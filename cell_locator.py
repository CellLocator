#!/usr/bin/env python
"""
GSM Cell logger based on mickeyterm.

(C) 2002-2006 Chris Liechti <cliecht@gmx.net>
(C) 2008 Michael 'Mickey' Lauer <mlauer@vanille-media.de>
(C) 2008 Baruch Even <baruch@ev-en.org>

GPLv3 or later
"""

__version__ = "0.2.4"

import sys, os, serial, time, dbus, csv
import pygtk
pygtk.require('2.0')
import gtk, gobject
import pango

debug = True

from dbus.mainloop.glib import DBusGMainLoop
DBusGMainLoop(set_as_default=True)

def arfcn_to_freq(data):
    arfcn = int(data)
    if 172 < arfcn and arfcn < 252: return 869 + (arfcn-127)*0.2
    if 511 < arfcn and arfcn < 811: return  1930 + (arfcn - 511) * 0.2
    if   1 < arfcn and arfcn <  124: return 935 + (arfcn * 0.2)
    if 974 < arfcn and arfcn < 1023: return 925 + (arfcn - 974) * 0.2
    if 511 < arfcn and arfcn <  886: return 1805 + (arfcn - 511) * 0.2
    return -1

class Terminal( object ):
    def __init__( self ):
        # try to get portname from MUXer
        import dbus
        bus = dbus.SystemBus()
        oMuxer = bus.get_object( "org.pyneo.muxer", "/org/pyneo/Muxer" )
        iMuxer = dbus.Interface( oMuxer, "org.freesmartphone.GSM.MUX" )
        port = iMuxer.AllocChannel( "celllocator.%d" % os.getpid() )
        assert port, "could not get path from muxer. need to supply explicit portname"
        print port
        self.serial = serial.Serial( str(port), 115200, rtscts=False, xonxoff=False, timeout=2)

    def open(self):
        self.serial.open()
        assert self.serial.isOpen(), "can't open serial port"

    def close(self):
        self.serial.close()

    def write(self, cmd):
        self.serial.write(cmd + '\r\n')
 
    def read( self ):
        chr = None
        s = ''
        while chr != '\n':
            chr = self.serial.read()
            if chr == '':
                return None
            if chr != '\n': s += chr
        while len(s) > 0 and s[-1] in ('\r', '\n'):
            s = s[0:-1]
        return s

    def exec_cmd(self, cmd):
        while 1:
            if debug: print 'Exec', cmd
            sys.stdout.flush()
            self.write(cmd)
            result = []
            restart = False
            while len(result) == 0 or (result[-1] != 'OK' and result[-1] != 'ERROR'):
                line = self.read()
                if debug: print 'DEBUG', line
                if line is None:
                    restart = True
                    break
                sys.stdout.flush()
                result.append(line)
            if restart:
                continue
            start = 0
            for start in range(len(result)):
                if debug: print 'DEBUG exec', result[start], result[start] == cmd
                if result[start] == cmd:
                    result = result[start+1:]
                    break
            return result

    def get_neighbour_cell_info(self):
        result = self.exec_cmd('AT%EM=2,3')
        count = int(result[0].split(' ')[1])
        cells = []
        hdrs = ('arfcn', 'c1', 'c2', 'rxlev', 'bsic', 'cell_id', 'lac', 'frame_offset', 'time_alignment', 'cba', 'cbq', 'cell_type_ind', 'rac', 'cell_resel_offset', 'temp_offset', 'rxlev_acc_min')
        for cell in range(count):
            d = {}
            for i in range(len(hdrs)):
		## Cannot be sure, that the "result" will have the right format, better catch the exceptions
		try:
                  d[hdrs[i]] = int(result[1+i].split(',')[cell])
		except:
		  d[hdrs[i]] = None
		  if debug: print "DEBUG:","ResponseSplitError:","splitting:",result[1+i]		
            cells.append(d)
        return cells

    def get_location_and_paging_info(self):
        result = self.exec_cmd('AT%EM=2,4')
        data = result[0].split(' ')[1].split(',')
        hdrs = ('bs_pa_mfrms', 't3212', 'mcc', 'mnc', 'tmsi')
        d = {}
        for i in range(len(hdrs)):
            d[hdrs[i]] = int(data[i])
        return d

    def get_service_cell_info(self):
        result = self.exec_cmd('AT%EM=2,1')
        data = result[0].split(' ')[1].split(',')
        hdrs = ('arfcn', 'c1', 'c2', 'rxlev', 'bsic', 'cell_id', 'dsc', 'txlev', 'tn', 'rlt', 'tav', 'rxlev_f', 'rxlev_s', 'rxqual_f', 'rxqual_s', 'lac', 'cba', 'cbq', 'cell_type_ind', 'vocoder')
        d = {}
        for i in range(len(hdrs)):
            d[hdrs[i]] = int(data[i])
        d['freq'] = arfcn_to_freq(d['arfcn'])
        return d


class GPS:
    def __init__(self):
        self.bus = dbus.SystemBus()
        self.objects = {}
        self.gps_obj = None
        self.usage_obj = None
        self.clear()

    def init(self):
        self.usage_obj = self.tryGetProxy( 'org.freesmartphone.ousaged', '/org/freesmartphone/Usage' )
        if not self.usage_obj:
            print 'ERROR: Failed getting the usage object'
            return False

        self.usage_iface = dbus.Interface(self.usage_obj, 'org.freesmartphone.Usage')
        self.usage_iface.RequestResource("GPS")

        self.gps_obj = self.tryGetProxy( 'org.freesmartphone.ogpsd', '/org/freedesktop/Gypsy' )
        if not self.gps_obj:
            print 'ERROR: GPS failed to initialize'
            return False

        self.gps_accuracy_iface = dbus.Interface(self.gps_obj, 'org.freedesktop.Gypsy.Accuracy')
        self.gps_accuracy_iface.connect_to_signal( "AccuracyChanged", self.cbAccuracyChanged )
        self.gps_position_iface = dbus.Interface(self.gps_obj, 'org.freedesktop.Gypsy.Position')
        self.gps_position_iface.connect_to_signal( "PositionChanged", self.cbPositionChanged )

    def clear(self):
        self.pdop = None
        self.hdop = None
        self.vdop = None
        self.timestamp = None
        self.lat = None
        self.lon = None
        self.alt = None

    def uninit(self):
        if self.usage_iface:
            self.usage_iface.ReleaseResource("GPS")

    def cbAccuracyChanged( self, fields, pdop, hdop, vdop ):
        #print 'Accuracy changed', fields, pdop, hdop, vdop
        self.fields = fields
        self.pdop = float(pdop)
        self.hdop = float(hdop)
        self.vdop = float(vdop)

    def cbPositionChanged( self, fields, timestamp, lat, lon, alt ):
        #print 'Position changed', fields, timestamp, lat, lon, alt
        self.fields = fields
        self.timestamp = int(timestamp)
        self.lat = float(lat)
        self.lon = float(lon)
        self.alt = float(alt)

    def tryGetProxy( self, busname, objname ):
        try:
            return self.bus.get_object( busname, objname )
        except DBusException, e:
            logger.warning( "could not create proxy for %s:%s" % ( busname, objname ) )

class GsmCellLogger:
    """Log GSM Cell location data, only logs when data is changed."""
    def __init__(self):
        self._f = None
        self.f = None
	self.logpoints = 0

    ext_keys = ('lac', 'cell_id', 'arfcn', 'bsic', 'rxlev', 'tav', 'time_alignment')

    def open(self, filename):
        self._f = file(filename, 'a')
        self.f = csv.writer(self._f)

        # Create and write a header for the CSV file
        row = ('record_type', 'timestamp', 'lon', 'lat', 'hdop', 'mcc', 'mnc') + self.ext_keys
        self.f.writerow(row)

        self.lastrow = None
	
	## Zero the number of logged points
	self.logpoints = 0


    def close(self):
        self._f.close()
        self._f = None
        self.f = None
        self.lastrow = None

    def log(self, ts, lat, lon, hdop, loc, cell, neighs):
        if hdop is None or float(hdop) > 3.0:
            # Skip logging if the gps data doesn't exist or is not accurate enough
            return

        row = (lat, lon, hdop, loc, cell, neighs)
        if row == self.lastrow:
            # Nothing that is worth writing about
            return
        self.lastrow = row

        def fields(d, keys):
            res = []
            for key in keys:
                res.append(d.get(key, -1))
            return res
        common = [ts, lon, lat, hdop, loc['mcc'], loc['mnc']]
        self.f.writerow([1] + common + fields(cell, self.ext_keys))
        for neigh in neighs:
            self.f.writerow([2] + common + fields(neigh, self.ext_keys))
        ## Increment number of logged points
	self.logpoints += 1

class DataCollector:
    def __init__(self):
        self.cancel_log = False
        self.cb = lambda x,y,z: None

    def init(self):
        self.gps = GPS()
        self.gps.init()

        self.t = Terminal()
        self.t.open()

        import datetime
        dtstr = datetime.datetime.now().strftime('%Y-%m-%d-%H-%M')

        self.cell_log = GsmCellLogger()
        self.cell_log.open('log_cells-%s.csv' % dtstr)
        gobject.timeout_add(5000, self.cell_logger)
        print 'Logging starts'

    def uninit(self):
        self.cancel_log = True

        self.cell_log.close()
        self.t.close()
        self.gps.uninit()

        self.cell_log = None
        self.t = None
        self.gps = None

    def callback_set(self, func):
        self.cb = func

    def cell_logger(self):
        if self.cancel_log:
            print 'Logging stopped'
            self.cancel_log = False
            return False

        print
        print self.gps.timestamp, self.gps.lat, self.gps.lon, self.gps.alt, self.gps.hdop, self.gps.vdop, self.gps.pdop
        neigh = self.t.get_neighbour_cell_info()
        loc = self.t.get_location_and_paging_info()
        cell = self.t.get_service_cell_info()
        print
        print 'Nearby Cells:'
        for ncell in neigh:
            print 'cell_id', ncell['cell_id']
            print 'bsic', ncell['bsic']
            print 'rxlev', ncell['rxlev']
            print 'lac', ncell['lac']
            print 'time_alignment', ncell['time_alignment']
            print
        print
        print 'This cell:'
        print 'cell_id', cell['cell_id']
        print 'lac', cell['lac']
        print 'tav', cell['tav']
        print 'bsic', cell['bsic']
        print 'rxlev', cell['rxlev']
        print 'mcc', loc['mcc']
        print 'mnc', loc['mnc']
        print
        self.cell_log.log(self.gps.timestamp, self.gps.lat, self.gps.lon, self.gps.hdop, loc, cell, neigh)
        self.cb(loc, cell, neigh)
        self.gps.clear()
        print 'done'
        sys.stdout.flush()
        return True

class Namespace: pass

class MainWindow:
    def __init__(self):
        self.data = DataCollector()

        self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        self.window.connect("delete_event", self.delete_event)
        self.window.connect("destroy", self.destroy)

	## Vertical box to align result table lines and buttons
	boxbig = gtk.VBox(False,0)

        def add_displine( box, label_text, controls):
	    """ Add full line in table: first header, then 7 columns for cell+neighbours """
	    widgets = []
	    hbox = gtk.HBox(True,0)
	    for index,ctl in enumerate(controls):
                if ctl == 'L':
                    label = gtk.Label(label_text[index])
	            label.modify_font(pango.FontDescription("Sherif 3 Bold"))
                    hbox.pack_start(label,True,True,0)
		else:
                    data_label = gtk.Label(label_text[index])
	            data_label.modify_font(pango.FontDescription("Sherif 4"))
		    widgets.append(data_label)
                    hbox.pack_start(data_label,True,True,0)
	    box.pack_start(hbox,True,True,0)
	    return widgets

	#### Button line on top: config, ..., ?
	box = gtk.HBox(True,0)
 	## Config button - for future use
	self.configbutton = gtk.Button('Configuration')
        #self.configbutton.connect("clicked", self.toggle_log, None)
	box.pack_start(self.configbutton,True,True,0)
	boxbig.pack_start(box,True,True,0)
     
        ######## Display fields
	## Column header
	self.header = add_displine(boxbig,['','C','N1','N2','N3','N4','N5','N6'],['L','L','L','L','L','L','L','L'])

	## Displayed values
        self.lac = add_displine(boxbig,    ['LAC','-','-','-','-','-','-','-'], ['L','','','','','','',''])
        self.cellid = add_displine(boxbig, ['Cell','-','-','-','-','-','-','-'],['L','','','','','','',''])
        self.bcch = add_displine(boxbig,   ['BCCH','-','-','-','-','-','-','-'],['L','','','','','','',''])
        self.bsic = add_displine(boxbig,   ['BSIC','-','-','-','-','-','-','-'],['L','','','','','','',''])
        self.rxlev = add_displine(boxbig,  ['RxL','-','-','-','-','-','-','-'], ['L','','','','','','',''])
        self.time_alignment = add_displine(boxbig, ['T/A','','-','-','-','-','-','-'],['L','','','','','','',''])

	## Multiparameter line: params either only for current cell, or same for every cells
        self.multipar = add_displine(boxbig, ['TxL','-','TAV','-','MCC','-','MNC','-'],['L','','L','','L','','L','',])

	## Displaying GPS info + number of logged points as the last field
	self.gpsline = add_displine(boxbig,['GPS','Lat','-','Lon','-','hdop','-','0'],['L','L','','L','','L','',''])
        ##########

	## Start log button - Toggle
	buttonbox = gtk.HBox(True,0)
	self.logbutton = gtk.ToggleButton('Log')
        self.logbutton.connect("clicked", self.toggle_log, None)
	buttonbox.pack_start(self.logbutton,True,True,0)

	## Exit button - only works when not logging!
	self.exitbutton = gtk.Button('Exit')
        self.exitbutton.connect("clicked", self.destroy, None)
	buttonbox.pack_start(self.exitbutton,True,True,0)
	boxbig.pack_start(buttonbox,True,True,0)
	
        self.data.callback_set(self.data_cb)

	self.window.add(boxbig)
	self.window.set_title('Cell Locator')
        self.window.show_all()


    def data_cb(self, loc, cell, neighbours):
	""" Display results in table: cell + neighbours"""
        self.lac[0].set_label(str(cell['lac']))
        self.cellid[0].set_label(str(cell['cell_id']))
        self.rxlev[0].set_label(str(cell['rxlev']))
        self.bcch[0].set_label(str(cell['arfcn']))
        self.bsic[0].set_label(str(cell['bsic']))
        self.multipar[0].set_label(str(cell['txlev']))
        self.multipar[1].set_label(str(cell['tav']))
        self.multipar[2].set_label(str(loc['mcc']))
        self.multipar[3].set_label(str(loc['mnc']))


	index = 0
        for ncell in neighbours:
            index += 1
            self.lac[index].set_label(str(ncell['lac']))
            self.cellid[index].set_label(str(ncell['cell_id']))
            self.rxlev[index].set_label(str(ncell['rxlev']))
            self.bsic[index].set_label(str(ncell['bsic']))
            self.bcch[index].set_label(str(cell['arfcn']))
            self.time_alignment[index].set_label(str(ncell['time_alignment']))

	## Erase labels if there are less than 6 neighbours
	if index < 6: 
	    for i in range(index+1,7):
               self.lac[i].set_label('-')
               self.cellid[i].set_label('-')
               self.rxlev[i].set_label('-')
               self.bsic[i].set_label('-')
               self.bcch[i].set_label('-')
               self.time_alignment[i].set_label('-')

	## Display GPS data
	if self.data.gps.lat != None :
            self.gpsline[0].set_label(str(round(self.data.gps.lat,3)))
        else:
	    self.gpsline[0].set_label('N/A')
	if self.data.gps.lon != None :
	    self.gpsline[1].set_label(str(round(self.data.gps.lon,3)))
        else:
	    self.gpsline[1].set_label('N/A')
	if self.data.gps.hdop != None :
	    self.gpsline[2].set_label(str(self.data.gps.hdop))
        else:
	    self.gpsline[2].set_label('N/A')

        ## Display number of logged points
	self.gpsline[3].set_label(str(self.data.cell_log.logpoints))

    def main(self):
        gtk.main()

    def toggle_log(self, widget, data=None):
	""" Toggle logging - and Exit/Config button availability """
        if widget.get_active():
            self.exitbutton.set_sensitive(False)
            self.configbutton.set_sensitive(False)
	    widget.set_label('Stop Log')
            self.data.init()
        else:
            self.data.uninit()
	    widget.set_label('Log')
	    self.exitbutton.set_sensitive(True)
	    self.configbutton.set_sensitive(True)

    def delete_event(self, widget, event, data=None):
        return False # Allow the window to close

    def destroy(self, widget, data=None):
        gtk.main_quit()


if __name__ == "__main__":
    mainwin = MainWindow()
    mainwin.main()


