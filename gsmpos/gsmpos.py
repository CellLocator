#!/usr/bin/env python
"""
GSM Positioning 

(C) 2008 Gergely Imreh <imrehg@gmail.com>
--- using code from: 
    cell_locator: Baruch Even <baruch@ev-en.org>

GPLv3 or later
"""
"""

  Uses input file: cellinfo.dat
  Contain        : Locations of known cells
  Format         : comma-separated values
  Fields         : mmc,mnc,cell_id,lattitude,longitude

"""


import sys, os, serial, time, dbus, csv
from time import strftime
from math import *
from Numeric import *
### GPS import functions from stripped down version of cell_locator.py
from cell_locator_bare import Terminal
from cell_locator_bare import GPS
import traceback

__version__ = '0.2'


###############

# Debugging output
debug = False

# Number of digits after decimal point when printing location
posdigits = 6

################

def triangulate(pos,idist):

    def middlep(pos1,idist1,pos2,idist2):
        """ Middle point of triangle side """
        return (pos1*idist1 + pos2*idist2)/(idist1+idist2)

    def perpvec(pos1,pos2):
        """ Vector perpendicular to the line connecting pos1 & pos2 """
        dir = pos2-pos1
        dir = array((-1*dir[1],dir[0]))
        return dir

    def inters(p1,p2,p3,p4):
        """ Calculating intersection point """
        try :
           ua = ((p4[0]-p3[0])*(p1[1]-p3[1])-(p4[1]-p3[1])*(p1[0]-p3[0]))/((p4[1]-p3[1])*(p2[0]-p1[0]) - (p4[0]-p3[0])*(p2[1]-p1[1]))
        except:
           ua = 0
        return p1+ua*(p2-p1)

    def getconsensus(inters):
        """ Calculate where to place most likely location,
            currently mean of the three intersections """
        x = 0
        y = 0
        for k in range(0,3):
           x += inters[k][0]/3.
           y += inters[k][1]/3.
        return x,y

    ## Get triangle side middle points
    mp00 =  middlep(pos[:,0],idist[0,0],pos[:,1],idist[0,1])
    mp10 =  middlep(pos[:,0],idist[0,0],pos[:,2],idist[0,2])
    mp20 =  middlep(pos[:,1],idist[0,1],pos[:,2],idist[0,2])
    ## Get another point on the line perpendicular to the sides
    mp01 = mp00+perpvec(pos[:,0],pos[:,1])
    mp11 = mp10+perpvec(pos[:,0],pos[:,2])
    mp21 = mp20+perpvec(pos[:,1],pos[:,2])
    ## Get intersections
    i1 = inters(mp00,mp01,mp10,mp11)
    i2 = inters(mp00,mp01,mp20,mp21)
    i3 = inters(mp10,mp11,mp20,mp21)
    i = array(((i1),(i2),(i3)))
    ## Calculate centre
    [x,y] =  getconsensus(i)
    return x,y


def getpos(cells,cellpresent,mcc,mnc):
	"""  Calculating position based on GSM cell data """
	""" 
 	 Signal converted to relative distances with assumptions:
	 - Equal tower power output
	 - Free space propagation: signal strength scales with d^(-1/2) 
         : These are of course not true - but there is a priori knowledge of proper signal strengh/distance scaling
	"""

	lon = 0
	lat = 0
	have = 0
        haveconnected = False	
        pos = zeros([2,len(cellpresent)], Float)        
        idist = zeros([1,len(cellpresent)], Float)
        sumweight = 0

	for k in range(len(cellpresent)):
            # Signal strength in dB -> linear scale 
            sig = 10**(int(cellpresent[k]['rxlev'])/10.0)
            # Linear scale -> distance scale (1/2 means assuming free space propagation)
            dist = sig**(-1/2.)
            if debug: print "DEBUG:",cellpresent[k]['cell_id'],":signal:",cellpresent[k]['rxlev'],"Sig-linear:",sig,"Distance scale:",dist
            for i in range(len(cells)):
	           if (mcc == cells[i]['mcc']) and (mnc == cells[i]['mnc']) and (cellpresent[k]['cell_id'] == cells[i]['cell_id']):
                        if k == 0 : haveconnected = True
                        pos[:,have] = [float(cells[i]['lat']),float(cells[i]['lon'])]
                        idist[0,have] = 1./dist
                        have += 1
                        if debug: print "DEBUG:",cellpresent[k]['cell_id'],":CellFound: ",cells[i]['lat'], cells[i]['lon']
                        break

        if have == 0:
            """ No guess if no tower """
            lat = None
            lon = None
        elif have == 1:
            """ Wild guess if one tower """
            lat = pos[0,0]
            lon = pos[1,0]
        elif have == 2:
            """ On a line if two towers """
            lat = (pos[0,0]/idist[0,0] + pos[0,1]/idist[0,1])/(1/idist[0,0]+1/idist[0,1])
            lon = (pos[1,0]/idist[0,0] + pos[1,1]/idist[0,1])/(1/idist[0,0]+1/idist[0,1])
        elif have == 3:
            """ Triangulate if three towers """
            [lat,lon] = triangulate(pos[:,0:3],idist[:,0:3])
        elif have > 3:
               """ If more than three : use cleverness! (hopefully) """
               lat = 0
               lon = 0
               if not haveconnected:
                   """ If the connected tower is not on the known list, move forward the tower
                       with the highest signal level to be first in the list """
                   signals = sort(idist)
                   for k in range(have):
                      if float(idist[0,k]) == float(signals[0][-1]):
                         maxsignal = k
                         break
                   templat = pos[0,0]
                   templon = pos[1,0]
                   tempidist = idist[0,0]
                   pos[0,0] = pos[0,k]
                   pos[1,0] = pos[1,k]
                   idist[0,0] = idist[0,k]
                   pos[0,k] = templat
                   pos[1,k] = templon
                   idist[0,k] = tempidist

               """ Triangulate possible triads (but keep the connected tower (or the highest signal one) 
                   for all of them), and select for those that are closest to the known tower position, 
                   finally average position """
               subpos = zeros([2,3], Float)        
               subidist = zeros([1,3], Float)
               subpos[:,0] = pos[:,0]
               subidist[0,0] = idist[0,0]
               numcalc = ((have-1)*(have-2)) / 2
               icalc = 0
               cpos = zeros([2,numcalc], Float)
               cdist = []
               for k in range(1,have):
                  for m in range(k+1,have):
                     subpos[:,1] = pos[:,k]
                     subpos[:,2] = pos[:,m]
                     subidist[0,1] = idist[0,k]
                     subidist[0,2] = idist[0,m]
                     [sublat,sublon] = triangulate(subpos,subidist)
                     cpos[0,icalc] = sublat 
                     cpos[1,icalc] = sublon
                     [dist, hdist, vdist] =  calcdist(pos[1,0],pos[0,0],sublon,sublat)
                     cdist.append(dist)
                     icalc += 1
               numuse = math.ceil(numcalc/2)
	       scdist = cdist[:]
               scdist.sort()
               uplimit = scdist[int(numuse)-1]
               for k in range(numcalc):
                   if  float(cdist[k]) <= (uplimit):
                       lat += float(cpos[0,k])
                       lon += float(cpos[1,k])
               lat = lat / numuse
               lon = lon / numuse

        else: 
            lat = None
            lon = None
	if debug: print "DEBUG:","Cells-found:",have
	if debug: print "DEBUG:","getpos:return:",lat,lon,have,len(cellpresent)
	return lat,lon,have,len(cellpresent)



def calcdist(lon1,lat1,lon2,lat2):
    """ Calculate (approximate) distance of two coordinates on Earth """
    """ 
      Returns distance, and North/South difference, Average East/West difference
      --- if lat2 / lon2 are more North / East : positive, if more South / East : negative 
    """
    # Earth mean radius (m)
    r = 6372795
    # From Wikipedia :)
    # Distance on sphere
    dist = r*acos( cos(lat1/180*pi)*cos(lat2/180*pi)*cos(lon1/180*pi - lon2/180*pi) + sin(lat1/180*pi)*sin(lat2/180*pi))
    # North/South difference
    ns = r*acos( cos(lat1/180*pi)*cos(lat2/180*pi) + sin(lat1/180*pi)*sin(lat2/180*pi))
    if (lat1 > lat2):
	ns = -1*ns
    # Usually don't need to, only at big distances: East/West difference, at both lattitudes, take the average
    ew1 = r*acos( cos(lat1/180*pi)*cos(lat1/180*pi)*cos(lon1/180*pi - lon2/180*pi) + sin(lat1/180*pi)*sin(lat1/180*pi))
    ew2 = r*acos( cos(lat2/180*pi)*cos(lat2/180*pi)*cos(lon1/180*pi - lon2/180*pi) + sin(lat2/180*pi)*sin(lat2/180*pi))
    avew = (ew1+ew2)/2.0
    if (lon1 > lon2):
	avew = -1*avew
    return dist,ns,avew


class GSMpos():
    """ Class to store GSM positioning information """
    def __init__( self):
	self.lat = None
	self.lon = None
	self.numtower = 0
	self.tottower = 0

def getgsmpos(gsmpos,gsmterm,cells):
	""" Positioning loop, based on cell_locator.py """
        neigh = gsmterm.get_neighbour_cell_info()
        loc = gsmterm.get_location_and_paging_info()
        cell = gsmterm.get_service_cell_info()
	
	mcc = loc['mcc']
	mnc = loc['mnc']
	cellpresent = []
	if cell['cell_id'] > 0:
		d = {}
		d['cell_id'] = cell['cell_id']
		d['rxlev'] = cell['rxlev']
		cellpresent.append(d)
		
	for k in range(len(neigh)):
		if neigh[k]['cell_id'] != 0:
			d = {}
			d['cell_id'] = neigh[k]['cell_id']
			d['rxlev'] = neigh[k]['rxlev']
			cellpresent.append(d)
	gsmpos.lat,gsmpos.lon,gsmpos.numtower,gsmpos.tottower = getpos(cells,cellpresent,mcc,mnc)
	

def looping(gsmterm,gsm,cells,gps=None):
      """ Displaying results """
      try:
	getgsmpos(gsm,gsmterm,cells)
	if (gsm.lat != None) and (gsm.lon != None):
		print "GSM: Pos:",round(gsm.lat,posdigits),",",round(gsm.lon,posdigits),"T:",gsm.numtower,"/",gsm.tottower
	else:   print "GSM: No fix"
        if gps != None:
           if (gps.lat != None) and (gps.lon != None):
              print "GPS: Pos:",round(gps.lat,posdigits),",",round(gps.lon,posdigits),"HDOP:",gps.hdop
           else:   print "GPS: No fix"
           if (gps.lat != None) and (gps.lon != None) and (gsm.lat != None) and (gsm.lon != None):
              dist,ns,ew = calcdist(gps.lon,gps.lat,gsm.lon,gsm.lat)
              print "GSM-GPS Diff: ",round(dist),"m"
              print "N/S : ",round(ns),"m | E/W: ",round(ew),"m"
           gps.clear()
        print ''
        sys.stdout.flush()
        return True
      except (KeyboardInterrupt, SystemExit):
        print "Keyboard Quit - please press Ctrl-C once more"
        return False
      except:
        etype = sys.exc_info()[0]
        evalue = sys.exc_info()[1]
        etb = traceback.extract_tb(sys.exc_info()[2])
        print "Exception in main loop: ", str(etype), " : ", str(evalue) , ' : ', etb
        print "Looping stopped - press Ctrl-c to quit"
        self_exit = 1
        return False


	

def loadcellinfo(celldatafile):
    """ Load cell information """
    cells = []
    try:
      f = open(celldatafile,'r')
      for line in f:
        params = ('mcc','mnc','cell_id','lat','lon')
	data = line.split(',')
	
	d = {}
	d[params[0]] = int(data[0])
	d[params[1]] = int(data[1])
	d[params[2]] = int(data[2])
	d[params[3]] = float(data[3])
	d[params[4]] = float(data[4])
	cells.append(d)
    except:
      etype = sys.exc_info()[0]
      evalue = sys.exc_info()[1]
      etb = traceback.extract_tb(sys.exc_info()[2])
      print "Error while loading GSM tower database: ", str(etype), " : ", str(evalue) , ' : ', str(etb)
    finally:
      return cells


if __name__ == "__main__":

    cells = loadcellinfo('cellinfo.dat')

    from dbus.mainloop.glib import DBusGMainLoop
    DBusGMainLoop(set_as_default=True)

    import gobject
    loop = gobject.MainLoop()
    context = loop.get_context()

    # Start GPS
    gps = GPS()
    gps.init()

    t = Terminal()
    t.open()

    # Init GSM position storage
    gsm = GSMpos()

    # Do positioning ever 3s
    self_exit = 0
    gobject.timeout_add(3000, looping, t, gsm, cells, gps)
    try:
        loop.run()
    except KeyboardInterrupt:
        print "Keyboard Quit"
        pass
    except:
        pass
      
    # Closing down
    print "Closing GSM"
    t.close()
    print "Closing GPS - wait for it!!"
    gps.uninit()
