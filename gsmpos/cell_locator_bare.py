#!/usr/bin/env python
"""
GSM Cell logger based on mickeyterm.

(C) 2002-2006 Chris Liechti <cliecht@gmx.net>
(C) 2008 Michael 'Mickey' Lauer <mlauer@vanille-media.de>
(C) 2008 Baruch Even <baruch@ev-en.org>
--- Gergely Imreh <imrehg@gmail.com>
    : few bugfixes
    : stripping down and only keeping structures relevant to GSMpos

GPLv3 or later
"""

__version__ = "0.2.4"

import sys, os, serial, time, dbus, csv

debug = False

def arfcn_to_freq(data):
    arfcn = int(data)
    if 172 < arfcn and arfcn < 252: return 869 + (arfcn-127)*0.2
    if 511 < arfcn and arfcn < 811: return  1930 + (arfcn - 511) * 0.2
    if   1 < arfcn and arfcn <  124: return 935 + (arfcn * 0.2)
    if 974 < arfcn and arfcn < 1023: return 925 + (arfcn - 974) * 0.2
    if 511 < arfcn and arfcn <  886: return 1805 + (arfcn - 511) * 0.2
    return -1

class Terminal( object ):
    def __init__( self ):
        # try to get portname from MUXer
        import dbus
        bus = dbus.SystemBus()
        oMuxer = bus.get_object( "org.pyneo.muxer", "/org/pyneo/Muxer" )
        iMuxer = dbus.Interface( oMuxer, "org.freesmartphone.GSM.MUX" )
        port = iMuxer.AllocChannel( "celllocator.%d" % os.getpid() )
        assert port, "could not get path from muxer. need to supply explicit portname"
        print port
        self.serial = serial.Serial( str(port), 115200, rtscts=False, xonxoff=False, timeout=2)

    def open(self):
        self.serial.open()
        assert self.serial.isOpen(), "can't open serial port"

    def close(self):
        self.serial.close()

    def write(self, cmd):
        self.serial.write(cmd + '\r\n')
 
    def read( self ):
        chr = None
        s = ''
        while chr != '\n':
            chr = self.serial.read()
            if chr == '':
                return None
            if chr != '\n': s += chr
        while len(s) > 0 and s[-1] in ('\r', '\n'):
            s = s[0:-1]
        return s

    def exec_cmd(self, cmd):
        while 1:
            if debug: print 'Exec', cmd
            sys.stdout.flush()
            self.write(cmd)
            result = []
            restart = False
            while len(result) == 0 or (result[-1] != 'OK' and result[-1] != 'ERROR'):
                line = self.read()
                if debug: print 'DEBUG', line
                if line is None:
                    restart = True
                    break
                sys.stdout.flush()
                result.append(line)
            if restart:
                continue
            start = 0
            for start in range(len(result)):
                if debug: print 'DEBUG exec', result[start], result[start] == cmd
                if result[start] == cmd:
                    result = result[start+1:]
                    break
            return result

    def get_neighbour_cell_info(self):
        result = self.exec_cmd('AT%EM=2,3')
        count = int(result[0].split(' ')[1])
        cells = []
        hdrs = ('arfcn', 'c1', 'c2', 'rxlev', 'bsic', 'cell_id', 'lac', 'frame_offset', 'time_alignment', 'cba', 'cbq', 'cell_type_ind', 'rac', 'cell_resel_offset', 'temp_offset', 'rxlev_acc_min')
        for cell in range(count):
            d = {}
            for i in range(len(hdrs)):
                d[hdrs[i]] = int(result[1+i].split(',')[cell])
            cells.append(d)
        return cells

    def get_location_and_paging_info(self):
        result = self.exec_cmd('AT%EM=2,4')
        data = result[0].split(' ')[1].split(',')
        hdrs = ('bs_pa_mfrms', 't3212', 'mcc', 'mnc', 'tmsi')
        d = {}
        for i in range(len(hdrs)):
            d[hdrs[i]] = int(data[i])
        return d

    def get_service_cell_info(self):
        result = self.exec_cmd('AT%EM=2,1')
        data = result[0].split(' ')[1].split(',')
        hdrs = ('arfcn', 'c1', 'c2', 'rxlev', 'bsic', 'cell_id', 'dsc', 'txlev', 'tn', 'rlt', 'tav', 'rxlev_f', 'rxlev_s', 'rxqual_f', 'rxqual_s', 'lac', 'cba', 'cbq', 'cell_type_ind', 'vocoder')
        d = {}
        for i in range(len(hdrs)):
            d[hdrs[i]] = int(data[i])
        d['freq'] = arfcn_to_freq(d['arfcn'])
        return d


class GPS:
    def __init__(self):
        self.bus = dbus.SystemBus()
        self.objects = {}
        self.gps_obj = None
        self.usage_obj = None
        self.timestamp = None
        self.pdop = None
        self.vdop = None
        self.hdop = None
        self.lat = None
        self.lon = None
        self.alt = None

    def init(self):
        self.usage_obj = self.tryGetProxy( 'org.freesmartphone.ousaged', '/org/freesmartphone/Usage' )
        if not self.usage_obj:
            print 'ERROR: Failed getting the usage object'
            return False

        self.usage_iface = dbus.Interface(self.usage_obj, 'org.freesmartphone.Usage')
        self.usage_iface.RequestResource("GPS")

        self.gps_obj = self.tryGetProxy( 'org.freesmartphone.ogpsd', '/org/freedesktop/Gypsy' )
        if not self.gps_obj:
            print 'ERROR: GPS failed to initialize'
            return False

        self.gps_accuracy_iface = dbus.Interface(self.gps_obj, 'org.freedesktop.Gypsy.Accuracy')
        self.gps_accuracy_iface.connect_to_signal( "AccuracyChanged", self.cbAccuracyChanged )
        self.gps_position_iface = dbus.Interface(self.gps_obj, 'org.freedesktop.Gypsy.Position')
        self.gps_position_iface.connect_to_signal( "PositionChanged", self.cbPositionChanged )

    def clear(self):
        self.pdop = None
        self.hdop = None
        self.vdop = None
        self.timestamp = None
        self.lat = None
        self.lon = None
        self.alt = None

    def uninit(self):
        if self.usage_iface:
            self.usage_iface.ReleaseResource("GPS")

    def cbAccuracyChanged( self, fields, pdop, hdop, vdop ):
        #print 'Accuracy changed', fields, pdop, hdop, vdop
        self.fields = fields
        self.pdop = pdop
        self.hdop = hdop
        self.vdop = vdop

    def cbPositionChanged( self, fields, timestamp, lat, lon, alt ):
        #print 'Position changed', fields, timestamp, lat, lon, alt
        self.fields = fields
        self.timestamp = timestamp
        self.lat = lat
        self.lon = lon
        self.alt = alt

    def tryGetProxy( self, busname, objname ):
        try:
            return self.bus.get_object( busname, objname )
        except DBusException, e:
            logger.warning( "could not create proxy for %s:%s" % ( busname, objname ) )

