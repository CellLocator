%%%%%%%%%%%%%%%%%%
%  (C) 2008 Gergely Imreh  <imrehg@gmail.com>
%
%  GPLv3 or later
%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%
% Crude Octave3 code to attempt to find GSM tower positions
% using log from Cell Locator:
% http://repo.or.cz/w/CellLocator.git
% http://projects.openmoko.org/projects/rocinante/
% 
% Very rough, trying to fit free-space propagation model to
% recorded signal. Also visualizing log data.
%%%%%%%%%%%%%%%%%%

function gsmfit()

%%%%%%%%%%%%
% Parameters to modify
%%%%%%%%%%%%
% GSM/GPS Logfile 
logfile = 'log_cells.csv';
% Clean up before use! Remove headers, and possible invalid lines

% Which GSM cell to check
cellcheck = 1;
% it is the number in the order of the cells listed in logfile 
% cellcheck = [1..number-of-different-cellid]

% Whether to plot: 0/1
toplot = 0;

%%%%End parameters

disp('Parameters ==============')
disp(['Logfile: ',logfile])
disp(['Cell to check : ' num2str(cellcheck)])

close all;
format none

%% Load logfile
data = load(logfile);


%% Parsing log into separate variables
% GPS position
lon = data(:,3); 
lat = data(:,4);
% CellID
cid = data(:,9);
% Base station id
bssc = data(:,10);
bsic = data(:,11);
bsid = data(:,10)*1000 + data(:,11);
% Received signal level
rxl = data(:,12);
% MCC/MNC numbers for the network
mcc = data(1,6);
mnc = data(1,7);

%% Getting all distinct CellID numbers 
bsidlist = unique(bsid);
%% Sometimes there's unknown cellid logged as '0' - omit
if (bsidlist(1) == 0)
    bsidlist = bsidlist(2:end);
end % if
disp(['Number of cells in log: ' num2str(length(bsidlist))])
disp(['MCC,MNC: ' num2str(mcc) ',' num2str(mnc)]);

if (cellcheck < 1) or (cellcheck > length(bsidlist))
	disp('Invalid cell number - no such cell to check')
end % if
	

% plot complete logged GPS track
if toplot == 1
	hold off
	figure(1)
	plot(lon,lat,'g.')
	axis equal
	hold on
end % if

out = [];

%% Do fitting
%%%%% In loop to allow for later extending to batch procession
%%%%% Currently only one at a time: need of visual inspection of data for each cell
for k = cellcheck:cellcheck

	%% Current CellID
	id = bsidlist(k);
	bssc_k = floor(id/1000);
	bsic_k = id - bssc_k*1000;
	disp(['MCC,MNC,Combined ID,BSIC,BSSC: ' num2str(mcc) ',' num2str(mnc) ',' num2str(id) ',' num2str(bsic_k) ',' num2str(bssc_k) ])
    
	%% Number of logged points
	nlog = length(bsid(bsid == id));
	disp(['Logged points: ' num2str(nlog)]);
	if nlog == 1
		disp('Only one point in data, cannot calculate anything');
		exit;
	end

	%% Select logged parameters for just this cell
	clat =  lat(bsid == id);
	clon =  lon(bsid == id);
	crxl =  rxl(bsid == id);
	cbs = bsic(bsid == id);
	basestations = unique(cbs); 
	disp(["Base stations: " num2str(length(basestations))])

	%% Turn RxL (dB) into signal scale
	csig =  10.^(crxl/10);

	% Calculate all signal compared to maximum recorded
	crxlnorm = max(crxl)-crxl;
	% Distance scaling power: d^(-dsq) : dsq=2 is free space propagation
	dsc = 2;
	%%%% scale factor only for plotting, that should be handled more cleverly...
	dscale = 0.00001;
	
	%% Approximate distance scale:
	% RxL (dB) -> Signal (relative) -> Distance (with assumed scaling)
	ds = 1./(10.^(crxlnorm/10)).^(1/dsc);
	dist = dscale*1./ds;

	%% Plot data recorded for given cell 
	% Location helper: 
	%   - circles around logged positions
	%   - circle radius is inversely proportional to signal strength
	%   ---> tower 'close': small circle; tower 'far': big circle
	if toplot == 1
		figure(1)
		plot(clon,clat,'b.')
		for l = 1:nlog
			c1 = dist(l)*cos(linspace(0,2*pi))+clon(l);
			c2 = dist(l)*sin(linspace(0,2*pi))+clat(l);
			plot(c1,c2)
		end % for
		axis equal
	end % if


	%% For fitting, guess the tower position close to the max recorded signal
	%% an add random offset 
	[maxsig maxsignum] = max(crxl);
	offsetm = 0.003;
	fitofflon = offsetm*rand-offsetm/2;
	fitofflat = offsetm*rand-offsetm/2;
	b_in = [clon(maxsignum)+fitofflon clat(maxsignum)+fitofflat 0.01];

	%% use the RxL for fitting...
	[yhat, b_est] = leasqr([clon clat],crxl,b_in,"gsmsurf",0.00001,3000);
	disp(['Fitted tower coordinates (Lat/Lon):  ' num2str(b_est(2)) ',' num2str(b_est(1))]);
	
	% Residual sum of squares (for fit quality purposes: smaller the better!
	RSSin = sum((gsmsurf([clon clat],b_in)-crxl).^2);
	RSSest = sum((gsmsurf([clon clat],b_est)-crxl).^2);
	disp(['RSS: ' num2str(RSSin) ' -> ' num2str(RSSest)])

	%%% Put fitted tower position on map: big red X
	if toplot == 1
		plot(b_est(1),b_est(2),'rx','MarkerSize',32);
		xlabel(['CellID:' num2str(id)  '/ RSS: ' num2str(RSSest) ' / Pos: ' num2str([b_est(1) b_est(2)])])
		axis equal;
		hold off
	end % if
	
	%% To assess fit quality: 
	if toplot == 1
		figure(2)
		lon0 = b_est(1);
		lat0 = b_est(2);
		r = sqrt((clon-lon0).^2 + (clat-lat0).^2);
%		yhat = yhat./log(10)*10;
		plot(r,crxl,'@',r,yhat,'x')
%		plot(r,lcsig,'@',r,yhat,'x')
		ylabel('RxL (dB)')
		xlabel(['Distance from fitted tower position / CellID:' num2str(id) ])
	end % if

	disp(['GSMPOS: ' num2str(mcc) ',' num2str(mnc) ',' num2str(id) ',' num2str(b_est(2)) ',' num2str(b_est(1)) ])
	disp(['OpenLayers: ' num2str(b_est(2)) '|' num2str(b_est(1)) '|cell ' num2str(id) '|mcc ' num2str(mcc) ' mnc ' num2str(mnc) '|icon_blue.png|24,24|0,-24'])
end % for
end % function gsmfit


%%%%%%
%%% Additional sub-routines
%%%%%%

function yhat = gsmsurf(X,b)
%% Signal propagation surface according to propagation assumption

	%% Assumed distance scaling: d^(-dsc): dsc=2 is free-space propagation
	dsc = 2;
    	%%%%% Separating input parameters into variables
    	% proposed cell position
	lon0 = b(1);
	lat0 = b(2);
	% tower output scale factor - "signal at tower" -  purely for fitting
	s0 = b(3);
	%%%%%

	% Distance from checked centre
	r = sqrt((X(:,1)-lon0).^2 + (X(:,2)-lat0).^2);
	% Estimated 'signal'
	yhat = 10*log(s0*r.^(-dsc))/log(10);
end

%%%%%%%%% END

gsmfit
