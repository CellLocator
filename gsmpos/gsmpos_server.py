#!/usr/bin/env python
"""
GSM Positioning

(C) 2008 Gergely Imreh <imrehg@gmail.com>
--- using code from: 
    cell_locator: Baruch Even <baruch@ev-en.org>

GPLv3 or later
"""
"""

  Uses input file: cellinfo.dat
  Contain        : Locations of known cells
  Format         : comma-separated values
  Fields         : mmc,mnc,cell_id,lattitude,longitude

"""


import sys, os, serial, time, dbus, csv
from time import strftime
from math import *
import sqlite3
### GPS import functions from stripped down version of cell_locator.py
from cell_locator_bare import Terminal
from cell_locator_bare import GPS
from gsmpos import *

__version__ = '0.2'


###############

# Debugging output
debug = False

################


def nmealocform(pos,padd=2):
    """ Convert location into NMEA format """
    if pos == '':
       nmeapos = ''
    else :
       a = int(pos)
       b = (pos-a)*60.0
       nmeapos = str(a).zfill(padd)+'%02.5f'%b
    return nmeapos

def nmeasentence(gsmpos,hdop=99):
    """ Prepare minimal information needed for tangoGPS location display """
    """ Shows: location, date, time, number of seen/known towers (at seen/fixed satellite fields """
    """ for info on sentences: http://gpsd.berlios.de/NMEA.txt and check gpsd output """

    nmeatime = strftime("%H%M%S.00")
    nmeadate = strftime("%d%m%y")

    lon = gsm.lon
    lat = gsm.lat
    numfixtower = gsm.numtower
    numtottower = gsm.tottower

    if (lon == None) or (lat == None):
        ## When no GSM position: send 'Navigation device warning' and 'no fix' sentences
        sentence  = "$GPRMC,"+nmeatime+",V,,,,,0.0,0.0,"+nmeadate+",,\n"
        sentence += '$GPGGA,'+strftime("%H%M%S.00")+',,,,,0,,,0,M,0,M,,\n'
        sentence += "$GPGSV,1,0,,,,,,,,,,,,,,,,,\n"
    else:
        if (lon >= 0):
            ew = "E"
            printlon = nmealocform(lon,3)
        else:
            ew = "W"
            printlon = nmealocform(-1*lon,3)
        if (lat >= 0):
            ns = "N"
            printlat = nmealocform(lat,2)
        else:
            ns = "S"
            printlat = nmealocform(-1*lat,2)
        sentence = ''
        sentence += "$GPRMC,"+nmeatime+",A,"+printlat+","+ns+","+printlon+","+ew+",0.0,0.0,"+nmeadate+",,\n"
        sentence += '$GPGGA,'+strftime("%H%M%S.00")+','+printlat+','+ns+','+printlon+','+ew+',1,'+str(numfixtower)+','+str(hdop)+',0,M,0,M,,\n'
        sentence += "$GPGSV,1,1,"+str(numtottower)+",,,,,,,,,,,,,,,,\n"

    return sentence 

def addtangogpsdatabase():
   """ Adding known cell towers to tangoGPS POI database """
   """ Tagged as Services/Other, with a common keywords field for easy fitering """ 
   import sqlite3

   cells = loadcellinfo('cellinfo.dat')
   try:
      conn = sqlite3.connect('/.tangogps/poi.db')
      c = conn.cursor()
      # First delete everything, clean start every time
      c.execute('delete from poi where keywords like "gsmpos%"')
      for cline in cells:
         lat = cline['lat']
         lon = cline['lon']
         comment = 'MCC: '+str(cline['mcc'])+', MNC:'+str(cline['mnc'])
         key = 'gsmpos CellID '+str(cline['cell_id'])
         indat = (lat,lon,key,comment,1,10,6,3,0)
         c.execute('insert into poi (lat,lon,keywords,desc,visibility,cat,subcat,price_range,extended_open) values (?,?,?,?,?,?,?,?,?)', indat)
   except:
      etype = sys.exc_info()[0]
      evalue = sys.exc_info()[1]
      etb = sys.exc_info()[2]
      print "tangoGPS POI Add Error: ", str(etype), " : ", str(evalue) , ' : ', str(etb)
   finally:
      conn.commit()
      c.close()


if __name__ == "__main__":

    cells = loadcellinfo('cellinfo.dat')

    from dbus.mainloop.glib import DBusGMainLoop
    DBusGMainLoop(set_as_default=True)

    import gobject
    loop = gobject.MainLoop()

    # Start GPS
    gps = GPS()
    gps.init()

    t = Terminal()
    t.open()

    # Init GSM position storage
    gsm = GSMpos()

    # Adding known cells to tangoGPS POI database
    addtangogpsdatabase()

##################
#  Quick and dirty server section
#################
    import socket

##########
    HOST = 'localhost'
    PORT = 2940 
##########

    s = None
    for res in socket.getaddrinfo(HOST, PORT, socket.AF_UNSPEC, socket.SOCK_STREAM, 0, socket.AI_PASSIVE):
            af, socktype, proto, canonname, sa = res
            try:
	       s = socket.socket(af, socktype, proto)
               s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)  # Important for using again after unexpected crash!
            except socket.error, msg:
	       s = None
               continue
            try:
               s.bind(sa)
               s.listen(1)
            except socket.error, msg:
               s.close()
               s = None
               continue
            break

    if s is None:
            print 'could not open socket'
            sys.exit(1)
    while 1: 
        print "Awaiting connection..."
        run = 1
	try: 
           conn, addr = s.accept()
        except:
           break

        print 'Connected by', addr
        while 1:

            try :
              # If manual connection, have to press a button to start, tangoGPS automatic
              data = conn.recv(1024)    
            except:
              conn.close()
              run = 0

            if data:
              while 1:
                try:
                  looping(t,gsm,cells)
                  nmeaout = nmeasentence(gsm)
                  print nmeaout
                  conn.send(nmeaout)
                  time.sleep(2.0)
		except (KeyboardInterrupt, SystemExit):
                  conn.close()
                  print "KeyboardInterrupt - Clean Exit"
                  run = 0
                  break
                except :
                  conn.close()
                  print "Exception:", sys.exc_type, ":", sys.exc_value
                  run = 0
                  break
            else:
               break
            print "Level 2"	
            if run ==0 : break
        print "Level 1"	
        if run ==0 : break
    print "Level 0"	


    # Closing down
    print "Closing GSM"
    t.close()
    print "Closing GPS - wait for it!!"
    gps.uninit()

    


